function multiplication(nombre) {
    let tab = [];
    for (i = 0; i <= 10; i++) {
        tab.push(i * nombre);
    }
    console.log(tab);
}


function randomMinMax(min, max) {
    let result = Math.round(Math.random() * (max - min) + min);
    return result;
}

function randomNumber() {
    let tab = [];
    for (i = 0; i < 10; i++) {
        tab.push(randomMinMax(1, 100));
    }
    //console.log(tab);
}

function readArray() {
    let tab = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    for (i = 0; i <= tab.length; i++) {
        let result = tab[i];
        console.log(result);
    }
}

function decomposition() {
    let word = "Anticonstitutionnellement";
    for (i = 0; i < word.length; i++) {
        let result = word[i].split('');
        console.log(result);
    }
}

function magicbool2() {
    let result = randomMinMax(1, 8);
    let tab = ["Essaye plus tard", "Essaye encore", "Pas d'avis", "C'est ton destin", "Le sort en est jeté", "Une chance sur deux", "Repose ta question", "D'après moi oui"];
    console.log(tab[result]);
}

function chiffresLettres() {
    let consonnes = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z'];
    let voyelles = ['a', 'e', 'i', 'o', 'u', 'y'];
    let result = [];

    for (i = 0; i <= 5; i++) {
        result.push(consonnes[randomMinMax(0, consonnes.length - 1)]);

    }
    for (let j = 0; j <= 3; j++) {
        result.push(voyelles[randomMinMax(0, voyelles.length -1)]);

    }
    return result;
}



function melangeur(tab){
   // tabTest = ['A', 'B', 'C', 'D'];
    let newTab = [];
   
    let longueur = tab.length;
    while(longueur > 0){
        longueur--;
        let random = randomMinMax(0, tab.length -1);
        newTab.push(tab.splice(random,1)[0]);  
    }
    return newTab;
}